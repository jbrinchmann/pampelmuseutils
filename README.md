# Pampelmuse utilities

This is a very simple python class to get data from
Pampelmuse results.

Everything is in the utilities package (at least for now) and
the documentation is severely lacking but here is at least a
quick summary for now:

## Installation

Go to where you want to install this and do

`git clone git@bitbucket.org:jbrinchmann/pampelmuseutils.git`

or

`git clone
https://jbrinchmann@bitbucket.org/jbrinchmann/pampelmuseutils.git`

Then go into `pampelmuseutils` and do `python setup.py install` or if
you want to work on it perhaps `pip install -e`.

## Usage

The package just contains a few routines to deal with output from
PampelMuse, the `prm` file and the individual spectra.

### Loading PSF information

The `get_psf_parameters` routine allows access to the observed
and fitted PSF parameters.

```
In [1]: import pampelmuseutils.utilities as pu                                   In [2]: import matplotlib.pyplot as plt
In [3]: prmfile = 'DATACUBE_FINAL.prm.fits'
In [4]: fit, obs = pu.get_psf_parameters(prfmfile)
In [5]: plt.scatter(obs['wave'], obs['fwhm'])
In [6]: plt.plot(fit['wave'], fit['fwhm'])
In [7]: plt.show()
```

### Load object information

This routine loads the table of objects used by PampelMuse.

```
In [1]: import pampelmuseutils.utilities as pu                                   In [2]: prmfile = 'DATACUBE_FINAL.prm.fits'
In [3]: t = pu.get_objects(prmfile)
In [4]: print(t)
```

If you want the table in Pandas format, you can set `pandas=True`.
```
In [3]: t = pu.get_objects(prmfile, pandas=True)
```

### Load all spectra in one go

The spectra are stored in the `.prm` file as well and
can be loaded easily

```
In [1]: import pampelmuseutils.utilities as pu                                   In [2]: prmfile = 'DATACUBE_FINAL.prm.fits'
In [3]: wave, flux, dflux = pu.get_spectra(prmfile)
```


### Load one single spectrum

PampelMuse also saves individual spectra to separate FITS files.
The format for loading thes is fairly simple:

```
In [1]: import pampelmuseutils.utilities as pu                                   In [2]: spfile = 'spectra/idid000002113jd2458163p6789f002.fits'
In [3]: sp = pu.get_single_spectrum(spfile)
```

The returned result of the `get_single_spectrum` routine is a
dictionary which contains the following keys:

- flux: this is the flux of the spectrum
- dflux: the uncertainty on the flux
- wave: the wavelength of the spectrum in Angstrom
- info: a dictionary with information on the source
- obsinfo: a dictionary with information on the observing conditions.


### Reporting on the PSF and S/N 

In the reporting subpackage there is a function for giving a quick
summary of a single PRM file:
```
[In [1]: import pampelmuseutils.reporting as pur

In [2]: df = pur.create_psf_report("DATACUBE_ZAP.prm.fits", include_sn=True)
DATACUBE_ZAP.prm.fits   FWHM   MedSN   MaxSN  N_SN>=3  N_SN>=5  N_SN>=20
DATACUBE_ZAP.prm.fits   0.537   1.56  199.54    47       25       12
```

The routine returns a data frame with this information. If multiple
PRM files are provided as input the dataframe has one line per file. 


### Coadding spectra

Thre is a small package for simple co-addition of spectra. An example
use to add all spectra of objects with magnitude between 22 and 23 is: 
```
In [1]: import pampelmuseutils.utilities as pu

In [2]: import pampelmuseutils.coadd as puc

In [3]: wave, flux, dflux = pu.get_spectra("DATACUBE_ZAP-ColumbaI.prm.fits")

In [4]: sps = {'wave': wave, 'flux': flux, 'dflux': dflux}

In [5]: t = Table().read('combined_table.fits')

In [6]: inds = np.where((t['MAG'] > 22) & (t['MAG'] < 23))

In [7]: spc, ys, dys = puc.add_spectra(sps, inds[0], normalise="7000-7500")
```

And we can see the results of this by for instance doing:
```
In [8]: import matplotlib.pyplot as plt

In [9]: Nl, Nobj = ys.shape

In [10]: plt.plot(spc['wave'], spc['flux'])
Out[10]: [<matplotlib.lines.Line2D at 0x1403c55b0>]

In [11]: for i in range(Nobj):
    ...:     plt.plot(spc['wave'], ys[:, i], color='gray', alpha=0.3)
    ...: 

In [12]: plt.plot(spc['wave'], spc['flux'])
Out[12]: [<matplotlib.lines.Line2D at 0x140337680>]

In [13]: plt.show()

```
