#!/usr/bin/env python

# -*- encoding: utf-8 -*-
from __future__ import absolute_import
from __future__ import print_function

from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

#
# Note that I have not included a data_files or package_data directive
# in the call to setup. This is because for the moment at least I consider
# the size of the data to be too large to send with the package. 
#
# I have also opted for a src/ packaging layout - based on the blog by ionelmc:
#    https://blog.ionelmc.ro/2014/05/25/python-packaging/
# which I found logical.
#
setup(name='pamplemuseutils',
      version='0.1.0',
      description='Interacting with PampelMuse',
      long_description=long_description,
      long_description_content_type="text/markdown",
      url='http://github.com/<TOBEFILLED>',
      author='Jarle Brinchmann',
      author_email='jbrinchmann@gmail.com',
      license='GPLv3',
      packages=find_packages('src/'), # Check this works as desired!
      package_dir={'': 'src'},
      zip_safe=False,
      classifiers=['Development Status :: 3 - Alpha',
                       'Intended Audience :: Astronomers',
                       'Topic :: Data reduction :: MUSE',
                       'License :: GPL version 3',
                       'Programming Language :: Python :: 2.7',
                       'Programming Language :: Python 3']
          )

