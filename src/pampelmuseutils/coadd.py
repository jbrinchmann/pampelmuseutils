#
# Provide functions for co-adding spectra.
#

from astropy.io import fits
import numpy as np
from . import utilities as pu

cspeed = 299792.458 # km/s

def list_of_spectra_to_2d_array(sps_list):
    """ Convert a list of dicts to a 2D array

    This is an internal function primarily and acts to convert a list
    of dicts with individual spectra to a dict with a 2D numpy array
    with all the spectra.
    """

    N_l = len(sps_list[0]['flux'])
    N_obj = len(sps_list)

    y_out = np.zeros((N_l, N_obj))
    dy_out = np.zeros((N_l, N_obj))
    for i in range(N_obj):
        y_out[:, i] = sps_list[i]['flux']
        dy_out[:, i] = sps_list[i]['dflux']
    

    return {'wave': sps_list[0]['wave'], 'flux': y_out, 'dflux': dy_out}


def _make_sp(x, y, dy, y_rms):
    
    sp = {'wave': x, 'flux': y, 'dflux': dy, 'rms_flux': y_rms}
    return sp

def vshift(xobs, xout, y_in, dy_in, voff):
    """Interpolate linearly a spectrum onto a restframe wavelength given a velocity

    Parameters:
    -----------
    xobs : array_like
           The observed wavelengths of the spectra
    
    xout : array_like
           The output wavelengths to interpolate onto

    y_in : array_like
           The input flux array. It should have dimensions Nx x Nstars
    
    dy_in : array_like
           The input flux uncertainty array, same shape as y_in

    voff : array_like
           The velocity offset array. If None, no interpolation is done.
           The velocities are assumed to be in km/s


    Returns
    -------
    y : array_like
        The rest-frame flux array

    dy : array_like
        The rest-frame flux uncertainty array


    Comments
    --------
    This is currently very simple. It does not deal with NaNs, it uses linear interpolation
    and it assumes that all input spectra have the same x_obs. This is fine for MUSE data
    but it is not very generalisable.
    """
    if voff is None:
        y = y_in
        dy = dy_in
    else:
        # User linear interpolation to shift in velocity
        # 
        # I am assuming that voff is the l.o.s. velocity, so that
        #
        #  x_obs = x_true*(1+voff/c)
        #
        #
        y = np.zeros_like(y_in)
        dy = np.zeros_like(dy_in)
        
        for i in range(len(voff)):
            voff_this = voff[i]
            y_this = y_in[:, i]
            dy_this = dy_in[:, i]
        
            x_true_this = xobs/(1.0+voff_this/cspeed)
            y[:, i] = np.interp(xout, x_true_this, y_this)
            dy[:, i] = np.interp(xout, x_true_this, dy_this)


    return y, dy



def coadd_average(x, y_in, dy_in, voff=None):
    """Coadd using average

    Parameters:
    -----------
    x : array_like
        The input wavelength array

    y_in : array_like
        The input flux array. Expected to be N_wave x N_objects.

    dy_in : array_like
        The input flux uncertainty array. Same shape as `y_in`.

    Keywords:
    ----------
    voff : array_like or None
          The velocity offset to be applied to each spectrum in km/s.
          If not offset is to be applied, this can be set to None.


    Returns:
    --------
    sp : dict
         A dict with the co-added spectrum. It will have the following keys:
         `wave` - The wavelength array.
         `flux` - The flux array
         `dflux` - The propagated flux uncertainty array
         `rms_flux` - The standard deviation of the input flux arrays
    


    Comments:
    ---------
    This routine does a simple average over the second dimension. NaNs are ignored
    when doing the averages and other reductions over the second dimension
    
    """
    

    y, dy = vshift(x, x, y_in, dy_in, voff)
    N_l, N_obj = y.shape
    
    flux_out = np.nanmean(y, axis=1)
    rms_flux = np.nanstd(y, axis=1)

    dflux_out = np.sqrt(np.nanmean(dy**2, axis=1))

    return _make_sp(x, flux_out, dflux_out, rms_flux)


def coadd_median(x, y_in, dy_in, voff=None):
    """Coadd using a median

    See `coadd_average` for the documentation - the only difference is that this
    present routine uses `median` for collapsing over the second dimension and the
    `rms_flux` is given by a MAD and the `dflux` is using a very approximate scaling
    of the propagated uncertainty. 

    """

    y, dy = vshift(x, x, y_in, dy_in, voff)
    N_l, N_obj = y.shape


    # Note I use keepdism here to be able to broadcast correctly on the line below.
    flux_out = np.nanmedian(y, axis=1, keepdims=True)
    # The 1.4826 is to scale MAD to equivalent to standard deviation.
    rms_flux = 1.4826*np.nanmedian(np.abs(y-flux_out), axis=1)
    # Then drop
    flux_out = flux_out.flatten()

    # What is the right way to propagate uncertainties here?
    #
    # My preference would be a MCMC, but for now I included only an
    # approximate scaling of standard deviation - see
    # https://en.wikipedia.org/wiki/Median#Efficiency
     
    dflux_out = np.sqrt(np.nanmean(dy**2)*np.pi/2.0)

    return _make_sp(x, flux_out, dflux_out, rms_flux)


# Not implementing inverse variance or other weighting for now
# def coadd_invvar(x, y, dy, voff=None):


def normalise_spectrum_in_window(x, y, dy, l1, l2):
    """Normalise a spectrum in a wavelength window"""

    ii=np.where( (x >= l1) & (x <= l2))
    norm = np.median(y[ii])

    return y/norm, dy/norm, norm

def normalise_spectrum_full(x, y, dy):
    """Normalise across the full wavelength range"""
    norm = np.median(y)

    return y/norm, dy/norm, norm


def normalise_spectra(x, y_in, dy_in, normalise=None):

    if normalise is None:
        y = y_in
        dy = dy_in
    else:
        # there are different ways to normalise a spectrum.
        # Here we handle the simples: overall and in a window.

        y = np.zeros_like(y_in)
        dy = np.zeros_like(dy_in)
        N_l, N_obj = y.shape
        norms = np.zeros(N_obj)
        
        if normalise == 'full':
            for i in range(N_obj):
                this_y, this_dy, norm = normalise_spectrum_full(x, y_in[:, i], dy_in[:, i])
                y[:, i] = this_y
                dy[:, i] = this_dy
                norms[i] = norm
        elif isinstance(normalise, str):
            # Has to have the form <Wave 1>-<Wave 2>
            i_split = normalise.find("-")

            if i_split > 0:
                l1 = float(normalise[0:i_split])
                l2 = float(normalise[i_split+1:])
                for i in range(N_obj):
                    this_y, this_dy, norm = normalise_spectrum_in_window(x, y_in[:, i], dy_in[:, i], l1, l2)
                    y[:, i] = this_y
                    dy[:, i] = this_dy
                    norms[i] = norm
            else:
                print("The normalise argument must be of the form L1-L2")
                return None, None
        else:
            print("Unsupported normalisation method!")
            return None, None

    return y, dy, norms
            
    

def add_spectra(sps, inds, voff=None, method='average', normalise=None):
    """Add a set of spectra

    A simple routine to combine spectra into a coadded spectrum.
    """

    known_methods = {'average': coadd_average,
                     'median': coadd_median}
    
    # I expect the sps to contain an array of spectra - here we check for this.
    if isinstance(sps, list):
        print("Please convert the list of spectra to a dict with arrays using list_of_spectra_to_2d_array")
        return None

    try:
        # Define some convenient variables.
        x = sps['wave']
        y = sps['flux']
        dy = sps['dflux']
    except:
        print("Please provide a dict with flux, dflux and wave in it!")
        return None


    # Ok, we should be fine if we get here!
    if inds is not None:
        y = y[:, inds]
        dy = dy[:, inds]

    # The normalise_spectra does notthing if normalise is set to None
    y, dy, norm = normalise_spectra(x, y, dy, normalise=normalise)
        
    
    # Dispatch on the method
    if method in known_methods:
        sp = known_methods[method](x, y, dy, voff=voff)
    else:
        print("Unknown method "+method)
        print("The known methods = ", known_methods.keys())
        sp = None
    
        
    return sp, y, dy
    


