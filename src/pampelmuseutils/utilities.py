"""
A simple package combining various utility functions for use with PampelMuse
"""

import numpy as np
from astropy.io import fits
import matplotlib.pyplot as plt
from pampelmuse.psf import Variables
import pandas as pd
from astropy.table import Table

def str_list_to_list(l):
    
    if isinstance(l, str):
        out = [l]
    else:
        out = l

    return out
    

def get_psf_parameters(prmfile):
    """Given a PRM file return the PSF parameters"""

    hdul = fits.open(prmfile)

    # First let us get the wavelength axis.
    hdr = hdul['SPECTRA'].header

    # Converting to Angstrom.
    n_l, dl, l0 = hdr['NAXIS3'], hdr['CDELT3']*1e10, hdr['CRVAL3']*1e10
    
    wave = np.arange(n_l)*dl+l0
    
    # No need to reinvent the loader since PampleMuse has this already.
    # I just want some re-organisation of the data.
    t = Variables.from_hdu(hdul['PSFPARS'])

    hdul.close()
    
    # But I prefer to reformat this a bit.
    
    fwhm = t.data['fwhm']
    fwhm_fit = fwhm['fit'].values
    fwhm_data = fwhm['value'].values
    
    try:
        beta = t.data['beta']
        beta_fit = beta['fit'].values
        beta_data = beta['value'].values
    except:
        beta_fit = np.zeros_like(fwhm_fit)
        beta_data = np.zeros_like(fwhm_fit)


    try:
        e = t.data['e']
        e_fit = e['fit'].values
        e_data = e['value'].values
    except:
        e_fit = np.zeros_like(fwhm_fit)
        e_data = np.zeros_like(fwhm_fit)


    try:
        theta = t.data['theta']
        theta_fit = theta['fit'].values
        theta_data = theta['value'].values
    except:
        theta_fit = np.zeros_like(fwhm_fit)
        theta_data = np.zeros_like(fwhm_fit)


    
    fit = pd.DataFrame({'fwhm': fwhm_fit, 'wave': wave, 'beta': beta_fit,
                        'e': e_fit, 'theta': theta_fit})

    obs = pd.DataFrame({'fwhm': fwhm_data, 'wave': wave,
                        'beta': beta_data, 'e': e_data,
                        'theta': theta_data})


    return fit, obs


def get_objects(prmfile, pandas=False):
    """Get the object catalogue from a PRM file

    The routine returns the object table as a astropy table 
    unless `pandas` is set to `True`, in which case a 
    pandas data frame is returned.

    """

    # Use astropy.table instead of FITS reading
    t = Table().read(prmfile, 'SOURCES')
    if (pandas):
        t = t.to_pandas()
    
    return t

def get_spectra(prmfile):
    """Get object spectra from a PRM file"""

    #-------------------------------------
    # First get the full set of spectra
    #-------------------------------------
    hdul = fits.open(prmfile)
    sp = hdul['SPECTRA'].data
    hdul.close()


    # Then the wavelength axis.
    hdr = hdul['SPECTRA'].header

    # Converting to Angstrom.
    n_l, dl, l0 = hdr['NAXIS3'], hdr['CDELT3']*1e10, hdr['CRVAL3']*1e10
    
    wave = np.arange(n_l)*dl+l0

    # Finally, I split things up for tidiness:
    flux = sp[:, :, 0]
    dflux = sp[:, :, 1]
    
    
    
    return wave, flux, dflux
    

def get_single_spectrum(spfile):
    """Load one single spectrum as extracted by GETSPECTRA

    Severely lacking in error checking etc!
    """

    # Open the file
    hdul = fits.open(spfile)

    # Get the flux and flux uncertainty
    flux = hdul['PRIMARY'].data
    dflux = hdul['SIGMA'].data

    # Get the header where there is a lot of useful information.
    hdr = hdul['PRIMARY'].header

    # Create wavelength array
    l = np.arange(hdr['NAXIS1'])*hdr['CDELT1']+hdr['CRVAL1']
    

    # Insert this information into one dict for observations
    # and one for info from PampelMuse
    obs_info = dict()
    other_info = dict()

    # These all needs HIERARCH OBSERVATION before the name
    obs_keys = ['AIRM START', 'AIRM END', 'AMBI FWHM START',
                    'AMBI FWHM END', 'AMBI PRES START',
                    'AMBI RHUM', 'AMBI TAU0', 'AMBI TEMP',
                    'AMBI WINDDIR', 'AMBI WINDSP']
    for k in obs_keys:
        try:
            obs_info[k] = hdr['HIERARCH OBSERVATION '+k]
        except:
            obs_info[k] = "Missing"

    # For other keys I take a slightly uglier approach
    for k in ['ID', 'RA', 'DEC', 'MAG']:
        try:
            other_info[k] = hdr['HIERARCH STAR '+k]
        except:
            other_info[k] = "Missing"

    for k in ['MULTISTAR', 'XCUBE', 'YCUBE', 'INFIELD',
                  'EDGEDIST', 'SNRATIO', 'MAG F606W',
                  'MAG DELTA', 'MAG ACCURACY', 'TYPE', 'QLTFLAG']:
        try:
            other_info[k] = hdr['HIERARCH SPECTRUM '+k]
        except:
            other_info[k] = "Missing"


    sp = {'obsinfo': obs_info, 'info': other_info, 'flux': flux,
              'dflux': dflux, 'wave': l}

    return sp


    
    
    
