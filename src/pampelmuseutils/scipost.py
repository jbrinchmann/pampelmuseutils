"""Convenience routines for running PampelMuse on individual exposures after
reduction with scipost"""

#
# The functioning of this is that this script creates a separate tree for
# Pampelmuse Runs.
#
# So we start with:
#
#  reduced/<version>/scipost/<DATES>
#
# And create a structure
#
#  pampelmuse/<version>/<DATES>/
#
# Where in each of the new <DATES> directories, we create a link for
# the DATACUBE_FINAL.fits from the reduced directory. Then we create
# a template pampelmuse settings file and a run_pampelmuse.sh script
# that later will be executed automatically. This script creates a
# done.remove_if_necessary file which signals that that directory has
# been processed.
#

import glob
import os.path
import os
import json

from . import utilities as pu
from . import reporting as pr

def _subdir(zap=False):

    if zap:
        subdir = "zap_scipost"
    else:
        subdir = "scipost"

    return subdir


def _top_outdir(zap=False):
    if zap:
        topdir = "pampelmuse_zap"
    else:
        topdir = "pampelmuse"

    return topdir

def get_dates_directories(version="0.1", use_analysed=False, zap=False):

    
    if use_analysed:
        topdir = _top_outdir(zap=zap)
        template = topdir+"/"+version+"/*"
    else:
        subdir = _subdir(zap=zap)
        template = "reduced/"+version+"/"+subdir+"/*"
    datedirs = glob.glob(template)
    
    dates = [os.path.basename(d) for d in datedirs]
    
    return {"dirs": datedirs, "dates": dates, "version": version}


def make_pampelmuse_script(info):

    make_pampelmuse_setup(info)


    text = """
PampelMuse pampelmuse-noninteractive.json INITFIT
PampelMuse pampelmuse-noninteractive.json CUBEFIT
PampelMuse pampelmuse-noninteractive.json POLYFIT
PampelMuse pampelmuse-noninteractive-nobin.json CUBEFIT
PampelMuse pampelmuse-noninteractive-nobin.json POLYFIT
PampelMuse pampelmuse-noninteractive-nobin.json GETSPECTRA
touch done.remove_if_necessary
    """
    
    with open("run_pampelmuse.sh", "w") as f:
        f.write(text)



def get_setup_dict(info):
        # Check that the user has provided those keys we do not
    # have defaults for.
    ok = 1
    if "catalogue" not in info:
        print("You need to specify the catalogue")
        ok = ok*0
    if "binning" not in info:
        print("You need to specify the binning")
        ok = ok*0

    if ok == 0:
        return None
        
    setup = {
        "global": {
            "interact": False,
            "prefix": info.get("prefix", "DATACUBE_FINAL"),
            "smallifu": False,
            "cpucount": -1
        },
        "catalog": {
            "name": info["catalogue"],
            "passband": info.get("passband", "F606W")
        },
        "initfit": {
            "usesigma": False,
            "snrcut": info.get("snrcut", 1),
            "psfmag": info.get("psfmag", 23.5),
            "posfit": info.get("posfit", 1),
            "apernois": info.get("apernois", 0.1)
        },
        "psf": {
            "profile": "moffat",
            "fit": 6,
            "fwhm": 4.0
        },
        
        "cubefit": {
            "iterations": 10
        },
        "getspectra": {
            "prefix": info.get("name", "id")
        },
        "polynom": {
            "degree": info.get("polydegree", 2)
        },
        "layers": {
            "binning": info["binning"]
        },
        "sky": {
            "use": False,
            "binsize": info["binning"]
        }
    }


    return setup
    

def make_pampelmuse_setup(info):
    """Create the init file for PampelMuse"""

    info["binning"] = 50
    setup_bin = get_setup_dict(info)
    with open('pampelmuse-noninteractive.json', 'w') as outfile:
        json.dump(setup_bin, outfile, indent=4)
    
    info["binning"] = 1
    setup_nobin = get_setup_dict(info)
    with open('pampelmuse-noninteractive-nobin.json', 'w') as outfile:
        json.dump(setup_nobin, outfile, indent=4)
        
    

def get_datacube_prefix(info, zap=False):
    # Figure out the name of the data cube file.
    if zap:
        datacube_prefix = "DATACUBE_ZAP"
    else:
        datacube_prefix = "DATACUBE_FINAL"

    try:
        datacube_name  = info["prefix"]+'.fits'
    except:
        # Ok, the user has not provided anything so
        # we should set the prefix to avoid having to
        # pass around the ZAP indicator everywhere.
        datacube_name = datacube_prefix+".fits"
        info["prefix"] = datacube_prefix
        
    return datacube_name

def make_pampelmuse_hierarchy(info, zap=False):
    

    # Create the tree down to version if necessary
    top_outdir = _top_outdir(zap=zap)
    
    ROOT=top_outdir+"/"+info["version"]
    os.makedirs(ROOT, exist_ok=True)

    datacube_name = get_datacube_prefix(info, zap=zap)
    print("PREFIX=", info["prefix"])

    
    os.chdir(ROOT)
    # Create the date directories
    for i_d, d in enumerate(info["dates"]):

        # Create the directory
        DIR = d
        print("Creating {0}".format(DIR))
        os.makedirs(DIR, exist_ok=True)

        # Create the link.
        os.chdir(DIR)
        datacube_file = os.path.join("../../../", info["dirs"][i_d], datacube_name)

        try:
            os.symlink(datacube_file, datacube_name)
        except FileExistsError:
            pass
        except:
            raise

        # Create the script - this also creates the setup files
        make_pampelmuse_script(info)
        
        os.chdir("../")


def get_scipost_prm(info, zap=False):
    """Return a list of PRM files for the structure created above"""

    
    top = _top_outdir(zap=zap)
    ROOT=top+"/"+info["version"]

    prms = []

    datacube_name = get_datacube_prefix(info, zap=zap)
    for d in info["dates"]:
        fname = ROOT+"/"+d+"/"+datacube_name+".prm.fits"
        if os.path.isfile(fname):
            prms.append(fname)
        

    return prms


def create_stats_scipost(info):

    prms = get_scipost_prm(info)
    prms.sort()

    pr.create_psf_report(prms, include_sn=True)

    
    
