#
# Various utility functions for looking at Pamplemuse outputs.
#
import matplotlib.pyplot as plt
import numpy as np
from astropy.io import fits
from . import utilities as pu

def show_sn_distributions(prmfiles, arcsinh=False, log10=False):
    """Given a set of PRM files, illustrate the distribution
    of S/N values for the spectra using violin plots."""


    sns = []
    
    for i_file, prmfile in enumerate(prmfiles):
        # Read in the spectra and give statistics on S/N
        hdul = fits.open(prmfile)
        d = hdul['SPECTRA'].data
        nl, nsp, ndum = d.shape
        this_sns = np.zeros(nsp)
        for i in range(nsp):
            y = d[:, i, 0]
            dy = d[:, i, 1]
            ok = np.where(np.isfinite(y))
            if arcsinh:
                this_sns[i] = np.arcsinh(np.median(y[ok]/dy[ok]))
            elif log10:
                this_sns[i] = np.log10(np.median(y[ok]/dy[ok]))
            else:
                this_sns[i] = (np.median(y[ok]/dy[ok]))

        sns.append(this_sns)

    fig, ax = plt.subplots()
    ax.violinplot(sns,  showextrema=False, vert=True, showmedians=True)

    
    return fig, ax


def show_psf_curves_multi(prmfiles_or_str, ax=None, pix_scale=0.2,
                          labels=None):
    """Given a set of PRM files, show PSF curves.

    Input:

       prmfiles - str or list
            The PRM files to show

    Keywords:
       ax        - Matplotlib axes object. Default=None ie. create a new one
       pix_scale - The pixel scale to assume when converting to arcsec. Default=0.2
       labels    - A list of labels to provide for the plot. Should match the
                   number of PRM files
    """


    if isinstance(prmfiles_or_str, str):
        prmfiles = [prmfiles_or_str]
    else:
        prmfiles = prmfiles_or_str

    
    if ax is None:
        fig, ax = plt.subplots(ncols=1, nrows=2, figsize=(12, 14))
    else:
        if len(ax) < 2:
            print("This function requires at least two axes as it plots in the first two")
            return None


    if labels is not None:
        # Check that they match the prmfiles
        if len(labels) != len(prmfiles):
            print("When providing labels, those should have the same length as the prmfiles")
            return None
        
    ax[1].set_xlabel("Wavelength [Angstrom]")
    ax[0].set_ylabel("FWHM [arcsec] - poly fit")
    ax[1].set_ylabel("FWHM [arcsec] - observed")
    for i_p, p in enumerate(prmfiles):
        fit, obs  = pu.get_psf_parameters(p)

        if labels is not None:
            lbl = labels[i_p]
        else:
            lbl = ""
        
        ax[0].plot(fit['wave'], fit['fwhm']*pix_scale, label=lbl)
        ax[1].plot(fit['wave'], obs['fwhm']*pix_scale, label=lbl)


    if labels is not None:
        plt.legend()

    return ax
