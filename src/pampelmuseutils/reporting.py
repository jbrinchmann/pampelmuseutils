#
# A collection of routines that can be used to report on
# the results contained in multiple PRM files.
#

from astropy.io import fits
import numpy as np
import pandas as pd
from . import utilities as pu

def create_psf_report(prmfiles_or_str, wavelength=7000,
                      include_sn=False, pixelscale=0.2):
    """Create a tabular overview of the PSF properties of a set of 
    cubes processed by PampelMuse.

    wavelength indicates the wavelength at which the FWHM is to be reported. 
    """

    # This is to allow the user to provide a single string as well as
    # a list of strings as input.
    if isinstance(prmfiles_or_str, str):
        prmfiles = [prmfiles_or_str]
    else:
        prmfiles = prmfiles_or_str
    
    N_files = len(prmfiles)
    first = True
    for i_file, prmfile in enumerate(prmfiles):
        fit, obs = pu.get_psf_parameters(prmfile)

        i_close = np.argmin(np.abs(fit['wave']-wavelength))
        fwhm_fit = fit["fwhm"][i_close]*pixelscale

        infix = "{0}".format(len(prmfile))
        header_format = "{0:"+infix+"s}   FWHM "
        header = header_format.format(prmfile)
        line = "{0}  {1:6.3f}".format(prmfile, fwhm_fit)

        # Create a dict with the results which we will convert to Pandas later.
        r = {"file": prmfile, "FWHM": fwhm_fit}
        
        if include_sn:
            # Read in the spectra and give statistics on S/N
            hdul = fits.open(prmfile)
            d = hdul['SPECTRA'].data
            nl, nsp, ndum = d.shape
            sns = np.zeros(nsp)
            for i in range(nsp):
                y = d[:, i, 0]
                dy = d[:, i, 1]
                ok = np.where(np.isfinite(y))
                sns[i] = np.median(y[ok]/dy[ok])
        
            r['median_sn']= np.median(sns)
            r['max_sn'] = np.max(sns)
            r['n_gt3'] = len(np.where(sns >= 3)[0])
            r['n_gt5'] = len(np.where(sns >= 5)[0])
            r['n_gt20'] = len(np.where(sns >= 20)[0])

            line = line+"  {0:5.2f}  {1:6.2f}  {2:4d}     {3:4d}     {4:4d}".format(r['median_sn'], r['max_sn'],
                                                           r['n_gt3'], r['n_gt5'], r['n_gt20'])

            header = header + "  MedSN   MaxSN  N_SN>=3  N_SN>=5  N_SN>=20"

        if first:
            print(header)
        print(line)

        # Prepare for the data frame
        if first:
            result = dict()
            for k in r.keys():
                result[k] = [r[k] for i in range(N_files)]
        else:
            for k in r.keys():
                result[k][i_file] = r[k]
                
        first  = False
        
        

    df = pd.DataFrame(result)
    return df

            
